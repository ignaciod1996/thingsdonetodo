import task from '@/components/Task'

export default {
  name: 'waterfall',
  components: {
    task
  },
  data() {
    return {

    }
  },
  computed: {
    tasks_by_category() {
      return this.$store.getters.tasksByCategory;
    }
  },
}