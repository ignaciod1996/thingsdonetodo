export default {
  name: 'task',
  props: {
    innerTask: Object
  },
  data() {
    return {
      open: false
    }
  },
  computed: {
      taskOpen () {
          return this.open ? 'open' : '';
      }
  },
  methods: {
    changeTaskStatus() {
      this.$store.dispatch('CHANGE_TASK_STATUS', this.innerTask.ID);
    }
  }
}