export default {
  name: 'categoryAside',
  data() {
    return {

    }
  },
  computed: {
    categories() {
      return this.$store.getters.categories;
    },
    totalPercent() {
      return this.$store.getters.totalTaskFinish;
    }
  },
  methods: {
    moveToCategory(category) {
      const el = document.querySelector(`#anchor_${category}`);
      if (el) el.scrollIntoView({blobk: 'end', behavior: 'smooth'});
    }
  }
}