import * as service from '../service'

const state = {
  user: {}
}

const getters = {
  getUser: state => state.user,
}

const actions = {
  FETCH_USER({ commit }) {
    return new Promise(resolve => {
      const userDataRaw = {
        method: 'GET',
        params: {
          nickname: 'Naaach',
          password: 'pass'
        }
      }

      service.getData('/getUser', userDataRaw)
        .then(response => {
          commit('setUser', response)
          resolve(response)
        })
        .catch(err => {
          console.log(err)
          resolve(null)
        })
    })
  }
}

const mutations = {
  setUser(state, payload) {
    console.log('user user user', payload);
    
    state.user = Object.seal(payload)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}