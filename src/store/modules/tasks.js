import * as service from '../service'

//  /////////////////////////////////////////////////////
//  STATE
//  /////////////////////////////////////////////////////
const state = {
  tasks: [],
}

//  /////////////////////////////////////////////////////
//  GETTERS
//  /////////////////////////////////////////////////////
const getters = {
  _tasks: state => state.tasks,
  tasks: state => {
    const parents = state.tasks.filter(t => t.ID_PARENT == null)

    function findChilds(parent_id) {
      const childs = state.tasks.filter(t => t.ID_PARENT == parent_id)
      if (!childs.length) return []

      childs.forEach((c, i) => childs[i].CHILDS = findChilds(c.ID))

      return childs
    }

    return parents.map(p => ({...p, CHILDS: findChilds(p.ID)}))
  },
  categories: state => {
    return state.tasks.reduce((cats, task) => {
      return (cats.indexOf(task.CATEGORY) == -1) ? [...cats, task.CATEGORY] : cats;
    }, []).sort((a, b) => a.localeCompare(b))
  },
  totalTaskFinish: state => {
    const taskLength = state.tasks.length;
    const finishLength = state.tasks.reduce((finish, task) => {
      return task.FINISH ? finish+1 : finish
    }, 0);

    return Math.floor( (finishLength*100) / taskLength );
  },
  tasksByCategory: (state, getters) => {
    return getters.tasks
    .sort((a, b) => a.CATEGORY.localeCompare(b.CATEGORY))
    .reduce((byCategory, task) => {
      if(!byCategory[task.CATEGORY]) {
        byCategory[task.CATEGORY] = [task];
      } else {
        byCategory[task.CATEGORY].push(task);
      }

      return byCategory;
    }, {});
  }
}

//  /////////////////////////////////////////////////////
//  ACTIONS
//  /////////////////////////////////////////////////////
const actions = {
  async FETCH_TASKS({ commit, getters }) {
    return new Promise(resolve => {
      const data = {
        method: 'GET',
        params: {
          user_id: getters.getUser.ID
        }
      }

      service.getData('/tasks', data).then(response => {
        commit('_setTasks', response)
        resolve(response)
      })
    })
  },
  CHANGE_TASK_STATUS({ commit, getters }, task_id) {
    const task_pos = getters._tasks.findIndex(task => task.ID == task_id);
    if (task_id != -1) {
      commit('_changeTaskStatus', task_pos);
    }
  }
}

//  /////////////////////////////////////////////////////
//  MUTATIONS
//  /////////////////////////////////////////////////////
const mutations = {
  _setTasks(state, payload) {
    state.tasks = payload;
  },
  _changeTaskStatus(state, payload) {
    state.tasks[payload].FINISH = ! state.tasks[payload].FINISH
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}