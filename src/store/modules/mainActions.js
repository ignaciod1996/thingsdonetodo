const state = {}
const getters = {}

const actions = {
  MAIN_FETCH({ dispatch }) {
    return new Promise(resolve => {
      Promise.resolve()
        .then(() => dispatch('FETCH_USER'))
        .then(() => dispatch('FETCH_TASKS'))
        .finally(() => resolve())
    })
  }
}

const mutations = {}

export default {
  state,
  getters,
  actions,
  mutations
}