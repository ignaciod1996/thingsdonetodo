const api_base_url = 'http://192.168.1.10:5000'

/**
 * 
 * @param {String} url hacia donde ha de apuntar en la api 
 * @param {object} options  { method: [GET, POST, PUT, DELETE], params: { param1: 'wololo' } }
 */

export const getData = function(url, options) {
  return new Promise((resolve, reject) => {
    if (!url || url.trim() == '') reject('No url')
    if (!options.method || options.method.trim() == '') reject('No method')

    let formedUrl = api_base_url + url

    // GET
    if (options.method.toUpperCase() == 'GET') {
      formedUrl += '?'
      Object.keys(options.params).forEach((paramKey, index) => {
        if (index > 0) formedUrl += '&'
        
        formedUrl += `${paramKey}=${options.params[paramKey]}`
      })
    }

    // POST

    // PUT

    // DELETE

    // SEND
    console.log('toApi', formedUrl);
    
    fetch(formedUrl, { mod: 'cors' }).then(
      response => resolve(response.json()),
      error => reject(error)
    )
  })
}