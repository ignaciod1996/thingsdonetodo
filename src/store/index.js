import Vue from 'vue'

import Vuex from 'vuex'
Vue.use(Vuex)

// Modules
import mainActions from './modules/mainActions';
import tasks from './modules/tasks';
import user from './modules/user';

export default new Vuex.Store({
  modules: {
    mainActions,
    tasks,
    user
  }
})
