import categoryAside from '@/components/CategoryAside'
import waterfall from '@/components/Waterfall'

export default {
  name: 'home',
  components: {
    categoryAside,
    waterfall
  }
}