import Vue from 'vue'

import Router from 'vue-router'
Vue.use(Router)

// Views
import Home from './views/Home'



export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    }
  ]
})
